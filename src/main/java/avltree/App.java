package avltree;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * http://practice.geeksforgeeks.org/problems/avl-tree-insertion/1
 *
 * Given a root of the tree you need to perform N AVL tree insertion operations on it. You need to complete the method insertToAVL which takes 2 arguments the first is the root of the tree and the second is the value of the node to be inserted.The function should return the root of the modified tree.

 Note: Your code will be checked for each insertion and will produce an output 1 if all the nodes for a particular test case were correctly inserted else 0.

 Input:
 The first line of input contains an integer T denoting the no of test cases. Then T test cases follow. Each test case contains 2 lines . The first line of each test case contains an integer N denoting the no of nodes to be inserted in the AVL tree and in the next line are N space separated values denoting the values of the nodes to be inserted to the tree.

 Output:
 For each test case output will be 1 if the node was correctly inserted else 0.

 Constraints:
 1<=T<=100
 1<=N<=100
 *
 */
public class App 
{
    public static void main( String[] args ) {
        System.out.println( "How many cases do you want? (1<=T<=100)" );
        Scanner input = new Scanner(System.in);
        int numberOfCases = Integer.valueOf(input.nextLine());
        Integer[] newRoot = new Integer[numberOfCases];
        List<List<Integer>> nodes = new ArrayList<>();

        for (int i = 0; i < numberOfCases; i++) {
            System.out.println("Enter the root of the case #" + (i+1));
            newRoot[i] = Integer.valueOf(input.nextLine());

            System.out.println("Enter values of nodes divided by space (1<=N<=100)");
            nodes.add(new ArrayList<Integer>());
            String stringOfNodes = input.nextLine();
            String[] arrayOfNodes = stringOfNodes.split("\\s");
            for (int j = 0; j < arrayOfNodes.length; j++) {
                nodes.get(i).add(Integer.valueOf(arrayOfNodes[j]));
            }

        }
        for (int i = 0; i < newRoot.length; i++) {
            System.out.println("The root after balancing is " + insertToAVL(newRoot[i], nodes.get(i)));
        }
    }

    public static Integer insertToAVL(Integer root, List<Integer> nodes) {
        AVLTree<Integer> newAvlTree = new AVLTree<>();
        newAvlTree.insert(root);
        for (Integer node : nodes) {
            boolean isSuccess = newAvlTree.insert(node);
            if (!isSuccess) {
                System.out.println(0);
                return newAvlTree.root.element;
            }
        }
        System.out.println(1);
        return newAvlTree.root.element;
    }
}
